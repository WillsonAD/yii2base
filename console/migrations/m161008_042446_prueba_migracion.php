<?php

use yii\db\Schema;
use yii\db\Migration;

class m161008_042446_prueba_migracion extends Migration
{
    public function up()
    {
        $this->createTable('prueba', [
            'id_prueba' => "MEDIUMINT(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY",
            'title' => "VARCHAR(255)  NOT NULL",
            'url' => "VARCHAR(255)  NOT NULL",
            'status' => "TINYINT(1) UNSIGNED  DEFAULT NULL",
            'id_user' => "MEDIUMINT(11) UNSIGNED  DEFAULT NULL",
        ]);
        $this->createIndex('index_id_prueba_url_status_id_user', 'prueba', ['id_prueba', 'url', 'status', 'id_user']);
    }
    public function down()
    {
        $this->dropTable('prueba');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
