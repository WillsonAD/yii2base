<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'suffix'=>'.ss',
            'rules'=>
            [

                '<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<id2:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\w+>/<id2:\w+>/<id3:\w+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

            ]
        ]
    ],
];
